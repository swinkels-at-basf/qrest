#####################################
API Base and Resources Processors
#####################################


.. automodule:: rest_client.resource

.. autoclass:: API
  :members:
  :private-members:
  :special-members: __init__

.. autoclass:: Resource
  :members:
  :private-members:
  :special-members: __init__

.. autoclass:: JSONResource
  :members:
  :private-members:
  :special-members: __init__


