__version__ = '2.99'

from .resource import JSONResource
from .conf import APIConfig, ResourceConfig, BodyParameter, QueryParameter
from .exception import  RestClientConfigurationError
from .resource import API

